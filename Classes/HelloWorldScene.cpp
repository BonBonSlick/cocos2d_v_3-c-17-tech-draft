#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "../proj.win32/AssetLoader.h"
#include "../proj.win32/PlayScene.h"

//using namespace cocos2d;
//USING_NS_CC;

cocos2d::Scene* HelloWorld::createScene()
{
	AssetLoader::initStart();

	return HelloWorld::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	//////////////////////////////
	// 1. super init first
	if (!cocos2d::Scene::init())
	{
		return false;
	}

	const cocos2d::Director* const director = cocos2d::Director::getInstance();
	cocos2d::Size const visibleSize = director->getVisibleSize();
	cocos2d::Vec2 const origin = director->getVisibleOrigin();




	/////////////////////////////
	// 2. add a menu item with "X" image, which is clicked to quit the program
	//    you may modify it.

	// add a "close" icon to exit the progress. it's an autorelease object
	cocos2d::MenuItemImage* closeItem = cocos2d::MenuItemImage::create(
		"CloseNormal.png",
		"CloseSelected.png",
		CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));

	if (closeItem == nullptr ||
		closeItem->getContentSize().width <= 0 ||
		closeItem->getContentSize().height <= 0)
	{
		problemLoading("'CloseNormal.png' and 'CloseSelected.png'");
	}
	else
	{
		float x = origin.x + visibleSize.width - closeItem->getContentSize().width / 2;
		float y = origin.y + closeItem->getContentSize().height / 2;
		closeItem->setPosition(cocos2d::Vec2(x, y));
	}

	// create menu, it's an autorelease object
	cocos2d::Menu* menu = cocos2d::Menu::create(closeItem, NULL);
	menu->setPosition(cocos2d::Vec2::ZERO);
	this->addChild(menu, 1);

	/////////////////////////////
	// 3. add your codes below...

	// add a label shows "Hello World"
	// create and initialize a label
	cocos2d::TTFConfig labelConfig;
	labelConfig.fontFilePath = "fonts/Marker Felt.ttf";
	labelConfig.fontSize = 24;
	labelConfig.glyphs = cocos2d::GlyphCollection::DYNAMIC;
	labelConfig.outlineSize = 2;
	labelConfig.customGlyphs = nullptr;
	labelConfig.distanceFieldEnabled = false;
	cocos2d::Label* label = cocos2d::Label::createWithTTF(labelConfig, "Hello World!");
	label->enableShadow(cocos2d::Color4B::GREEN);
	label->enableOutline(cocos2d::Color4B::RED, 1);
	label->enableGlow(cocos2d::Color4B::ORANGE);
	if (label == nullptr)
	{
		problemLoading("'fonts/Marker Felt.ttf'");
	}
	else
	{
		// position the label on the center of the screen
		label->setPosition(
			cocos2d::Vec2(
				origin.x + visibleSize.width / 2,
				origin.y + visibleSize.height - label->getContentSize().height
			)
		);

		// add the label as a child to this layer
		this->addChild(label, 1);
	}

	// add "HelloWorld" splash screen"
	//auto sprite = Sprite::create("HelloWorld.png");

	// @todo - IMPORTANT!!! no error if no such image!
	cocos2d::SpriteFrame* newspriteFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("dude.png");
	cocos2d::Sprite* sprite = cocos2d::Sprite::createWithSpriteFrame(newspriteFrame);

	if (sprite == nullptr)
	{
		problemLoading("'dude'");
	}
	else
	{
		// position the sprite on the center of the screen
		sprite->setPosition(cocos2d::Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
		// add the sprite as a child to this layer
		this->addChild(sprite, 0);

		// MoveBy - lets move the sprite by 500 on the x axis over 2 seconds
		// MoveBy is relative - since x = 200 + 500 move = x is now 700 after the move
		auto moveBy = cocos2d::MoveBy::create(1, cocos2d::Vec2(-111, sprite->getPositionY()));
		// create a  Ease Action
		auto moveByEaseOut = cocos2d::EaseOut::create(moveBy->clone(), 1.0f);
		auto moveTo = cocos2d::MoveTo::create(1, cocos2d::Vec2(visibleSize.width / 2 + origin.x, sprite->getPositionY()));
		auto moveToEaseIn = cocos2d::EaseIn::create(moveTo->clone(), 1.0f);
		// Delay - create a small delay
		auto delay = cocos2d::DelayTime::create(1);
		auto seq = cocos2d::Sequence::create(moveByEaseOut, delay, moveToEaseIn, nullptr);
		sprite->runAction(seq);

		auto rotateBy = cocos2d::RotateBy::create(2.0f, 60.0f);
		sprite->runAction(rotateBy);
		// Scale uniformly by 3x over 2 seconds
		auto scaleBy = cocos2d::ScaleBy::create(2.0f, 3.0f);
		sprite->runAction(scaleBy);

		// fades out the sprite in 2 seconds
		auto fadeOut = cocos2d::FadeOut::create(3.0f);
		sprite->runAction(fadeOut);
		// fades in the sprite in 1 seconds
		auto fadeIn = cocos2d::FadeIn::create(3.0f);
		sprite->runAction(fadeIn);
		auto seq2 = cocos2d::Sequence::create(fadeOut, delay->clone(), fadeIn, nullptr);
		sprite->runAction(seq2);

		// now lets animate the sprite we moved
		auto newspriteFrame1 = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("dude.png");
		auto newspriteFrame2 = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("earth.png");
		cocos2d::Vector<cocos2d::SpriteFrame*> animFrames;
		animFrames.reserve(2);
		/*animFrames.pushBack(SpriteFrame::create("dude.png", Rect(0, 0, 65, 81)));
		animFrames.pushBack(SpriteFrame::create("earth.png", Rect(0, 0, 65, 81)));*/
		animFrames.pushBack(newspriteFrame1);
		animFrames.pushBack(newspriteFrame2);
		// create the animation out of the frames
		cocos2d::Animation* animation = cocos2d::Animation::createWithSpriteFrames(animFrames, 0.5f);
		cocos2d::Animate* animate = cocos2d::Animate::create(animation);
		// run it and repeat it forever
		sprite->runAction(cocos2d::RepeatForever::create(animate));

		// create a few actions.
		auto jump = cocos2d::JumpBy::create(0.5, cocos2d::Vec2(0, 0), 100, 1);
		// create a few callbacks
		auto callbackJump = cocos2d::CallFunc::create([]() {
			cocos2d::log("Jumped!");
			});
		// create a sequence with the actions and callbacks
		auto seq3 = cocos2d::Sequence::create(jump, callbackJump, nullptr, nullptr, nullptr);
		// run it
		sprite->runAction(seq3);

		auto moveBy2 = cocos2d::MoveBy::create(10, cocos2d::Vec2(155, 55));
		auto fadeTo2 = cocos2d::FadeTo::create(2.0f, 120.0f);
		// running the above Actions with Spawn.
		auto mySpawn = cocos2d::Spawn::createWithTwoActions(moveBy2, fadeTo2);
		sprite->runAction(mySpawn);

		auto moveBy3 = cocos2d::MoveBy::create(3, cocos2d::Vec2(55, 55));
		auto moveBy4 = cocos2d::MoveBy::create(3, cocos2d::Vec2(-155, -155));
		auto mySpawn2 = cocos2d::Spawn::createWithTwoActions(moveBy3, moveBy4);
		auto seq4 = cocos2d::Sequence::create(mySpawn2, delay->clone(), mySpawn2->clone(), nullptr, nullptr);
		sprite->runAction(seq4->reverse());



		auto emitter = cocos2d::ParticleFireworks::create();
		emitter->setPosition(cocos2d::Vec2(visibleSize.width / 2, 0));

		addChild(emitter, 10);

		auto emitter2 = cocos2d::ParticleFireworks::create();
		// set the duration
		emitter2->setDuration(cocos2d::ParticleSystem::DURATION_INFINITY);
		// radius mode
		emitter2->setEmitterMode(cocos2d::ParticleSystem::Mode::RADIUS);
		// radius mode: 100 pixels from center
		emitter2->setStartRadius(100);
		emitter2->setStartRadiusVar(0);
		emitter2->setEndRadius(cocos2d::ParticleSystem::START_RADIUS_EQUAL_TO_END_RADIUS);
		emitter2->setEndRadiusVar(0);    // not used when start == end

		addChild(emitter2, 10);



		cocos2d::Sprite* sprite2 = cocos2d::Sprite::create("HelloWorld.png");
		// create ParallaxNode
		auto paraNode = cocos2d::ParallaxNode::create();
		// background image is moved at a ratio of 0.4x, 0.5y
		paraNode->addChild(sprite2, -1, cocos2d::Vec2(0.4f, 0.5f), cocos2d::Vec2::ZERO);
		/*// tiles are moved at a ratio of 2.2x, 1.0y
		paraNode->addChild(sprite2->clone(), 1, cocos2d::Vec2(2.2f, 1.0f), cocos2d::Vec2(0, -200));
		// top image is moved at a ratio of 3.0x, 2.5y
		paraNode->addChild(sprite2, 2, cocos2d::Vec2(3.0f, 2.5f), cocos2d::Vec2(200, 800));*/
		paraNode->setPosition(cocos2d::Vec2(visibleSize.width / 2, 0));
		paraNode->runAction(cocos2d::CCRepeatForever::create(cocos2d::MoveBy::create(5, cocos2d::Vec2(10, 0))));
	}



	/* CHANGE SCENE
	auto const scene2 = HelloWorld::createScene();
	director->replaceScene(scene2); */

	return true;
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
	auto  playScene = PlayScene::createScene();
	cocos2d::Director::getInstance()->replaceScene(
		cocos2d::TransitionFade::create(1, playScene, cocos2d::Color3B(0, 255, 255))
	);

	//	cocos2d::Director::getInstance()->pushScene(playScene);
	//Close the cocos2d-x game scene and quit the application
	//Director::getInstance()->end();

	/*To navigate back to native iOS screen(if present) without quitting the application,
	do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/

	//EventCustom customEndEvent("game_scene_close_event");
	//_eventDispatcher->dispatchEvent(&customEndEvent);


}
