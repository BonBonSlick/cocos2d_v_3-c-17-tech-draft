#include "PlayScene.h"
#include "ParalaxScene.h"
#include "SimpleAudioEngine.h"
#include "../proj.win32/AssetLoader.h"
#include "ui/CocosGUI.h"
#include <iostream>
#include "audio/include/AudioEngine.h"
using namespace cocos2d::experimental;
cocos2d::Scene* PlayScene::createScene()
{
	AudioEngine::stopAll();
	return PlayScene::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool PlayScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!cocos2d::Scene::init())
	{
		return false;
	}

	const cocos2d::Director* const director = cocos2d::Director::getInstance();
	cocos2d::Size const visibleSize = director->getVisibleSize();
	cocos2d::Vec2 const origin = director->getVisibleOrigin();


	/////////////////////////////
	// 2. add a menu item with "X" image, which is clicked to quit the program
	//    you may modify it.

	// add a "close" icon to exit the progress. it's an autorelease object
	cocos2d::MenuItemImage* closeItem = cocos2d::MenuItemImage::create(
		"CloseNormal.png",
		"CloseSelected.png",
		CC_CALLBACK_1(PlayScene::menuCloseCallback, this));

	if (closeItem == nullptr ||
		closeItem->getContentSize().width <= 0 ||
		closeItem->getContentSize().height <= 0)
	{
		problemLoading("'CloseNormal.png' and 'CloseSelected.png'");
	}
	else
	{
		float x = origin.x + visibleSize.width - closeItem->getContentSize().width / 2;
		float y = origin.y + closeItem->getContentSize().height / 2;
		closeItem->setPosition(cocos2d::Vec2(x, y));
	}

	// create menu, it's an autorelease object
	cocos2d::Menu* menu = cocos2d::Menu::create(closeItem, NULL);
	menu->setPosition(cocos2d::Vec2::ZERO);
	this->addChild(menu, 1);

	// Our .plist file has names for each of the sprites in it.  We'll grab
	// the sprite named, "mysprite" from the sprite sheet:
	// @todo - IMPORTANT!!! no error if no such image!
	/*auto newspriteFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("earth");
	auto sprite = cocos2d::Sprite::createWithSpriteFrame(newspriteFrame);

	/*auto newspriteFrame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName("dude");
	auto sprite = cocos2d::Sprite::createWithSpriteFrame(newspriteFrame);*/
	auto button = cocos2d::ui::Button::create("Button_Normal.png", "Button_Press.png", "Button_Disable.png");

	button->setTitleText("Some Very Long Long Button Text");
	button->setPosition(cocos2d::Vec2(
		origin.x + visibleSize.width / 2,
		origin.y + visibleSize.height / 2
	));

	button->addTouchEventListener([&](cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType type) {
		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::BEGAN:
			break;
		case cocos2d::ui::Widget::TouchEventType::ENDED:
			cocos2d::log("Button 1 clicked");
			break;
		default:
			break;
		}
		});

	this->addChild(button);


	auto loadingBar = cocos2d::ui::LoadingBar::create("LoadingBarFile.png");
	loadingBar->setPosition(cocos2d::Vec2(
		origin.x + visibleSize.width / 5,
		origin.y + visibleSize.height / 5
	));
	// set the direction of the loading bars progress
	loadingBar->setDirection(cocos2d::ui::LoadingBar::Direction::RIGHT);
	// something happened, change the percentage of the loading bar
	loadingBar->setPercent(25);
	// more things happened, change the percentage again.
	loadingBar->setPercent(35);
	this->addChild(loadingBar);

	/* CHANGE SCENE
	auto const scene2 = PlayScene::createScene();
	director->replaceScene(scene2); */

	return true;
}


void PlayScene::menuCloseCallback(Ref* pSender)
{
	cocos2d::Director::getInstance()->replaceScene(
		cocos2d::TransitionFade::create(1, ParalaxScene::createScene(), cocos2d::Color3B(0, 255, 255))
	);

	//Close the cocos2d-x game scene and quit the application
//	cocos2d::Director::getInstance()->end();

	/*To navigate back to native iOS screen(if present) without quitting the application,
	do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/

	//EventCustom customEndEvent("game_scene_close_event");
	//_eventDispatcher->dispatchEvent(&customEndEvent);
}
