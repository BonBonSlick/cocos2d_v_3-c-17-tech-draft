#include "AssetLoader.h"

#include "cocos2d.h"
using namespace cocos2d;

AssetLoader::AssetLoader()
{
}

AssetLoader::~AssetLoader()
{
}

void AssetLoader::initStart()
{
	// load the Sprite Sheet
	auto spritecache = SpriteFrameCache::getInstance();
	// the .plist file can be generated with any of the tools mentioned below
	spritecache->addSpriteFramesWithFile("sprites.plist");
}

