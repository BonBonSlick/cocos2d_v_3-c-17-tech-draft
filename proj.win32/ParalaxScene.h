#ifndef __PARALAX_SCENE_H__
#define __PARALAX_SCENE_H__

#include "cocos2d.h"

class ParalaxScene : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init() override;

	// a selector callback
	void menuCloseCallback(cocos2d::Ref* pSender);

	// implement the "static create()" method manually
	CREATE_FUNC(ParalaxScene);
};

#endif
